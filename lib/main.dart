import 'package:flutter/material.dart';
import 'package:flutter_provider_test/providers/AuthProvider.dart';
import 'package:flutter_provider_test/views/authorList.dart';
import 'package:flutter_provider_test/views/login.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MultiProvider(
    providers: [ChangeNotifierProvider(create: (_) => AuthProvider())],
    child: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  void _gotToAuthors() async {
    AuthProvider authProvider = Provider.of<AuthProvider>(context, listen: false);
    if (await authProvider.isLoggedIn()) {
      Navigator.push(context, MaterialPageRoute(builder: (context) => AuthorList()));
    } else {
      Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Center(
            child: Column(
          children: [
            ElevatedButton(
              child: Text('List authors'),
              onPressed: () {
                _gotToAuthors();
              },
            )
          ],
        )));
  }
}
