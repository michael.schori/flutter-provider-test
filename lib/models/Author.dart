class Author {
  int _id;
  String _firstname;
  String _lastname;
  String _imageUrl;

  Author();

  Author.named(
      this._id, this._firstname, this._lastname, this._imageUrl
      );

  static Author fromJson(dynamic json) {
    return Author.named(json['id'], json['first_name'], json['last_name'], 'https://icon-library.com/images/2018/3537945_pingu-photograph-hd-png-download.png');
  }

  String get imageUrl => _imageUrl;

  String get lastname => _lastname;

  String get firstname => _firstname;

  int get id => _id;
}
