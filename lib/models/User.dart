class User {
  String _userIsStoredKey = 'userIsStored';
  bool userIsStored;

  String _userUsernameKey = 'userUsername';
  String username;

  String _userPasswordKey = 'userPassword';
  String password;

  String _userTokenKey = 'userToken';
  String token;

  User({this.username, this.password, this.token});

  String get userIsStoredKey => _userIsStoredKey;

  String get userTokenKey => _userTokenKey;

  String get userPasswordKey => _userPasswordKey;

  String get userUsernameKey => _userUsernameKey;
}
