import 'dart:convert';

import 'package:flutter_provider_test/exceptions/LoginException.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:flutter_provider_test/models/User.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

enum Status {
  NotLoggedIn,
  LoggedIn,
}

class AuthProvider with ChangeNotifier {
  String _uriBase = 'https://m151-library-prepare.herokuapp.com/api/';
  String _uriToken = 'api-token-auth/';
  FlutterSecureStorage _storage = FlutterSecureStorage();
  Status _userStatus = Status.NotLoggedIn;
  User _user = User();

  Status get userStatus => _userStatus;

  User get user => _user;

  Future<String> getBearerToken() async {
    if (await this.isLoggedIn()) {
      return 'Bearer ' + _user.token;
    }
    throw LoginException();
  }

  Future<bool> isUserSaved() async {
    String isSaved = await _storage.read(key: _user.userIsStoredKey);
    return isSaved != null && isSaved.toLowerCase() == 'true';
  }

  Future<bool> isLoggedIn() async {
    if (await isUserSaved()) {
      await loadUser();
      await login(username: _user.username, password: _user.password);
    }
    return userStatus == Status.LoggedIn;
  }

  Future<bool> login({String username, String password}) async {
    _user.username = username;
    _user.password = password;

    var response = await http.post(Uri.parse(_uriBase + _uriToken),
        headers: <String, String>{'Content-Type': 'application/json; charset=UTF-8'},
        body: jsonEncode(<String, String>{'username': username, 'password': password}));

    var body = jsonDecode(response.body);

    if (body.containsKey('token')) {
      _userStatus = Status.LoggedIn;
      _user.token = body['token'];
      saveUser();
      return true;
    }
    return false;
  }

  Future<bool> logout() async {
    await deleteUser();
    _userStatus = Status.NotLoggedIn;
    return true;
  }

  Future<User> loadUser() async {
    if (await isUserSaved()) {
      _user = User();
      _user.username = await _storage.read(key: _user.userUsernameKey);
      _user.password = await _storage.read(key: _user.userPasswordKey);
      _user.token = await _storage.read(key: _user.userTokenKey);
      _user.userIsStored = await _storage.read(key: _user.userIsStoredKey) == 'true';
    }
    return _user;
  }

  Future<User> saveUser() async {
    await _storage.write(key: _user.userUsernameKey, value: _user.username);
    await _storage.write(key: _user.userPasswordKey, value: _user.password);
    await _storage.write(key: _user.userTokenKey, value: _user.token);
    _user.userIsStored = true;
    await _storage.write(key: _user.userIsStoredKey, value: _user.userIsStored.toString());
    return _user;
  }

  Future<bool> deleteUser() async {
    await _storage.delete(key: _user.userUsernameKey);
    await _storage.delete(key: _user.userPasswordKey);
    await _storage.delete(key: _user.userTokenKey);
    await _storage.delete(key: _user.userIsStoredKey);
    return true;
  }
}
