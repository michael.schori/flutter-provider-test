import 'package:flutter/material.dart';
import 'package:flutter_provider_test/models/User.dart';
import 'package:flutter_provider_test/providers/AuthProvider.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter_provider_test/models/Author.dart';
import 'package:provider/provider.dart';

class AuthorList extends StatefulWidget {
  const AuthorList({Key key}) : super(key: key);

  @override
  _AuthorListState createState() => _AuthorListState();
}

class _AuthorListState extends State<AuthorList> {
  final _authorListUrl = 'https://m151-library-prepare.herokuapp.com/api/authors/';

  Future<List<Author>> fetchAuthors() async {
    AuthProvider authProvider = Provider.of<AuthProvider>(context, listen: false);
    String bearerToken = '';
    try {
      bearerToken = await authProvider.getBearerToken();
    } catch (e) {
      Navigator.pop(context);
    }
    var result = await http.get(Uri.parse(_authorListUrl), headers: {'Authorization': bearerToken});

    List<Author> authors;
    authors = (json.decode(result.body) as List).map((e) => Author.fromJson(e)).toList();

    return authors;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Authors'),
        ),
        body: Container(
          child: FutureBuilder<List<Author>>(
              future: fetchAuthors(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.hasData) {
                  return ListView.builder(
                      padding: EdgeInsets.all(8),
                      itemCount: snapshot.data.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Card(
                          color: Colors.lightGreen[50],
                          child: Column(
                            children: [
                              ListTile(
                                leading: CircleAvatar(
                                  radius: 30,
                                  backgroundColor: Colors.black,
                                  backgroundImage: NetworkImage(snapshot.data[index].imageUrl),
                                ),
                                title: Text(snapshot.data[index].firstname),
                                subtitle: Text(snapshot.data[index].lastname),
                                trailing: Text(snapshot.data[index].id.toString()),
                              )
                            ],
                          ),
                        );
                      });
                } else {
                  return Center(child: CircularProgressIndicator());
                }
              }),
        ));
  }
}
