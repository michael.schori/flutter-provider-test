import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_provider_test/main.dart';
import 'package:flutter_provider_test/providers/AuthProvider.dart';
import 'package:provider/provider.dart';

class Login extends StatefulWidget {
  const Login({Key key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  bool _isLoading = false;

  final _formKey = GlobalKey<FormState>();
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();

  void initState(){
    super.initState();
    _isLoading = false;
  }

  @override
  void dispose() {
    _usernameController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  void _login() async {
    AuthProvider authProvider = Provider.of<AuthProvider>(context, listen: false);
    if (_formKey.currentState.validate()) {
      if (!await authProvider.login(username: _usernameController.text, password: _passwordController.text)) {
        _showSnackbar('Login not possible');
        setState(() {
          _isLoading = false;
        });
        return;
      }
      Navigator.pop(context);
    }
  }

  void _showSnackbar(String message) {
    final snackBar = SnackBar(
      content: Text(message),
      action: SnackBarAction(
        label: 'Close',
        onPressed: () {
          // TODO
        },
      ),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Login'),
        ),
        body: Form(
            key: _formKey,
            child: Padding(
              padding: EdgeInsets.all(20.0),
              child: Center(
                  child: (_isLoading == true)
                      ? CircularProgressIndicator()
                      : Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            TextFormField(
                              decoration: InputDecoration(
                                  contentPadding: EdgeInsets.all(20.0),
                                  border: OutlineInputBorder(
                                    borderRadius: const BorderRadius.all(
                                      const Radius.circular(24.0),
                                    ),
                                  ),
                                  labelText: 'Username',
                                  hintText: 'Enter your username'),
                              controller: _usernameController,
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Please enter your username';
                                }
                                return null;
                              },
                            ),
                            SizedBox(height: 30),
                            TextFormField(
                              obscureText: true,
                              decoration: InputDecoration(
                                  contentPadding: EdgeInsets.all(20.0),
                                  border: OutlineInputBorder(
                                    borderRadius: const BorderRadius.all(
                                      const Radius.circular(24.0),
                                    ),
                                  ),
                                  labelText: 'Password',
                                  hintText: 'Enter your password'),
                              controller: _passwordController,
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Please enter your password';
                                }
                                return null;
                              },
                            ),
                            SizedBox(height: 17),
                            ElevatedButton(
                              child: Text('Login'),
                              onPressed: () {
                                setState(() {
                                  _isLoading = true;
                                  _login();
                                });
                              },
                            ),
                            TextButton(
                              onPressed: () {},
                              child: Text(
                                'Forgot Password',
                                style: TextStyle(color: Colors.blue, fontSize: 15),
                              ),
                            ),
                          ],
                        )),
            )));
  }
}
